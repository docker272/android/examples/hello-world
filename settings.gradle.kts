pluginManagement {
    repositories {
        gradlePluginPortal()
        google()
        mavenCentral()
    }
}
dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        mavenCentral()
        maven {
            url = uri("https://gitlab.com/api/v4/projects/36665250/packages/maven")
        }
    }
}
rootProject.name = "Hello World Docker"
include(":app")
include("foobar")
project(":foobar").projectDir = file("foobar-module/foobar")
